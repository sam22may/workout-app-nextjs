import { useEffect, useState } from "react"
import pageAuth from "../../components/helper/pageAuth";
import { userService } from "../../components/services/user.crudApi"


const Profil = () => {
    const [email, setEmail] = useState("");

    useEffect(() => {
        const getStorage = localStorage.getItem('user')
        const userStorage = JSON.parse(getStorage)
        setEmail(userStorage.email)
    }, [])

    const handleLogout = () => {
        localStorage.removeItem('user')
        window.location.replace("/")
    }


    return (
        <div>
            <h1>Profil</h1>
            <p>{email}</p>
            <button type="button" onClick={handleLogout}>Logout</button>
        </div>
    )
}

export default pageAuth(Profil);