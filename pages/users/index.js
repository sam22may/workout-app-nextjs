import { userService } from "../../components/services/user.crudApi"

export const getStaticProps = async () => {


  // const res = await fetch("http://localhost:5000/api/users/")
  // const data = await res.json()

  const test = await userService.getAll()
  

  return {
    props: {
      users: test
    }
  }
}

const Users = ({ users }) => {
  return (
    <div className="black">
      <h1>All Users</h1>
      <div>
        {users.map(user => (
          <div key={user.id}>
            <p>{user.email}</p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Users;