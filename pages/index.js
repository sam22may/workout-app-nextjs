import Head from 'next/head'
import Image from 'next/image'
import { useState } from 'react';
import Layout from '../components/layout';
import { userService } from '../components/services/user.crudApi';
import LogoSDevWeb from '../components/svg/logoSDevWeb'
import styles from '../styles/Login.module.css'


export default function Login() {
  const [errormsg, setErrormsg] = useState("");

  const [inputs, setInputs] = useState({
    email: '',
    password: ''
  })
  const { email, password } = inputs

  function handleChange(e) {
    const { name, value } = e.target
    setInputs(inputs => ({ ...inputs, [name]: value }))
  }

  async function handleSubmit(e) {
    e.preventDefault()
    const login = await userService.login(inputs.email, inputs.password)
    if (!login) {
      setErrormsg(error.message)
    }
  }

  return (
    <Layout className={styles.container}>
      <main className={styles.main}>
        <h1 className={styles.title}>
          Workout app
        </h1>

        <form onSubmit={handleSubmit}>
          <p className={styles.red}>{errormsg}</p>
          <input
            type="email"
            className={styles.form_input}
            name="email"
            id="email"
            placeholder="email"
            value={email}
            onChange={handleChange}
            autoComplete="email"
          />
          <input
            type="password"
            className={styles.form_input}
            name="password" id="password"
            placeholder="password"
            value={password}
            onChange={handleChange}
            autoComplete="current-password"
          />
          <button type="submit" className={styles.loginBtn}>Enter</button>
        </form>
      </main>
    </Layout>
  )
}
