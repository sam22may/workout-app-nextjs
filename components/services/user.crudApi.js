import React from 'react';
import { authHeader } from "../helper/authHeader";


export const userService = {
    login,
    logout,
    register,
    getAll,
    getById,
    update,
    delete: _delete,
    handleResponse
};

function setUserStorage(user) {
    if(user.message) {
        return (
            localStorage.removeItem('user'),
            { message: "Email/password don't match" }
        )
    }
    // store user details and jwt token in local storage to keep user logged in between page refreshes
    localStorage.setItem('user', JSON.stringify(user));
    window.location.replace("/profil")
    return user;

}

async function login(email, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, password })
    };
    if(email && password) {
    return await fetch('http://localhost:5000/api/users/authenticate', requestOptions)
        .then(handleResponse)
        .then(user => setUserStorage(user))
        // .then(friendService.createFriendsList())
    } else {
        return { message: "Email and password are needed to login" }
    }
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
    window.location.reload(true);
}

async function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };

    return await fetch('http://localhost:5000/api/users', requestOptions).then(handleResponse);
}

async function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return await fetch(`http://localhost:5000/api/users/${id}`, requestOptions).then(handleResponse);
}

async function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    await fetch('http://localhost:5000/api/users/register', requestOptions).then(res => {
        console.log("ERROR : ");
        // if (res.status !== 200) {
        //     return setErrormsg("Invalid email");
        // }
    })

    return await fetch('http://localhost:5000/api/users/authenticate', requestOptions)
        .then(handleResponse)
        .then(user => setUserStorage(user))
}

async function update(user) {
    let requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    //Udpate the user on the server and send back a new
    await fetch(`http://localhost:5000/api/users/${user.id}`, requestOptions)
        .then(handleResponse)
        .then(user => setUserStorage(user))
}

// prefixed function name with underscore because delete is a reserved word in javascript
async function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return await fetch(`http://localhost:5000/api/users/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    // console.log("RESPONSE : ", response);
    return response.text().then(text => {
        const data = text && JSON.parse(text);

        if (response.status === 401) {
            // auto logout if 401 response returned from api
            logout();
        }

        return data;
    })
}