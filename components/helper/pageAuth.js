import { useEffect, useState } from "react";
import Login from "../../pages";

const pageAuth = Component => {
    const Auth = (props) => {
        // Login data added to props via localStorage (redux-store or use react context for example)
        const [user, setUser] = useState(null);
        useEffect(() => {
            if(localStorage){
                const storage = localStorage.getItem("user");
                setUser(storage)
            }
        }, []);

        // If user is not logged in, return login component
        if (!user) {
            return (
                <Login />
            );
        }

        // If user is logged in, return original component
        return (
            <Component {...props} />
        );
    };

    // Copy getInitial props so it will run as well
    if (Component.getInitialProps) {
        Auth.getInitialProps = Component.getInitialProps;
    }

    return Auth;
};

export default pageAuth;