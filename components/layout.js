import Head from "next/head"
import LogoSDevWeb from './svg/logoSDevWeb'
import styles from '../styles/layout.module.css'

export default function Layout({ children }) {
    return (
        <div>
            <Head>
                <title>Login - Workout App</title>
            </Head>
            <main>{children}</main>

            <footer className={styles.footer}>
                Powered by{' '}
                <span className={styles.logo}>
                    <LogoSDevWeb width={70} height={55} fill={"#f0f8ff"} />
                </span>
            </footer>
        </div>
    )
}