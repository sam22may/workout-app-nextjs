const config = require('config.json');
const mysql = require('mysql2/promise');
const { Sequelize, DataTypes } = require('sequelize');
const UserModel = require('../users/user.model');

module.exports = db = {};



initialize();

async function initialize() {
    // creation d'une bd si elle n'existe pas
    const { host, port, user, password, database } = config.database;
    const connection = await mysql.createConnection({ host, port, user, password });
    await connection.query(`CREATE DATABASE IF NOT EXISTS \`${database}\`;`);

    // connection à la bd
    const sequelize = new Sequelize(database, user, password, { dialect: 'mysql', host: "localhost" });

    // initialise le model et le lie à db
    db.User = require('../users/user.model')(sequelize);




    // synchronize all models with the database
    await sequelize.sync({ alter: true });
}

